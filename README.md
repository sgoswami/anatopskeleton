## https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopRun2WorkshopPreTutorial2020

# The recipe to set up the AnaTop package and run:

```
mkdir myanatop
cd myanatop
mkdir build source
setupATLAS --quiet
lsetup git
cd source
asetup AnalysisBase,21.2.183,here
cd ..
cd build
cmake ../source
cmake --build ./
source ./build/x86_64-centos7-gcc8-opt/setup.sh 
cd ..
mkdir run
cd run
cp $AnalysisBase_DIR/data/TopAnalysis/example-config.txt my-cuts-ljets.txt
vim my-cuts-ljets.txt
vim filelist.txt
top-xaod my-cuts-ljets.txt filelist.txt 
```
